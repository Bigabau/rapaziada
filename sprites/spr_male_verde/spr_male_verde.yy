{
    "id": "571a0448-bcbe-48be-9e33-e6b68028bf8a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_male_verde",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 17,
    "bbox_right": 43,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cdeaf72d-9d4e-4e1c-a76e-89b80ff88af8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "571a0448-bcbe-48be-9e33-e6b68028bf8a",
            "compositeImage": {
                "id": "1437a1ab-14a7-4d67-9102-fbd1c292f302",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cdeaf72d-9d4e-4e1c-a76e-89b80ff88af8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f549c49b-3f77-43c9-8fd7-9dc65f566f82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cdeaf72d-9d4e-4e1c-a76e-89b80ff88af8",
                    "LayerId": "9c06d140-09e1-4058-9154-b51a904189b1"
                }
            ]
        },
        {
            "id": "43df75f4-0e48-4e48-810b-16e08d3c1fd6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "571a0448-bcbe-48be-9e33-e6b68028bf8a",
            "compositeImage": {
                "id": "cb02d861-095f-4d7b-8f4f-330c114c2f80",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "43df75f4-0e48-4e48-810b-16e08d3c1fd6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39f88ab8-f108-479e-877c-194f6b8b96cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "43df75f4-0e48-4e48-810b-16e08d3c1fd6",
                    "LayerId": "9c06d140-09e1-4058-9154-b51a904189b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9c06d140-09e1-4058-9154-b51a904189b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "571a0448-bcbe-48be-9e33-e6b68028bf8a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 28,
    "yorig": 33
}