{
    "id": "204f2fe5-c253-4f11-b1d1-5779ca934d5d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "ZombieRage",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 272,
    "bbox_left": 235,
    "bbox_right": 683,
    "bbox_top": 234,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e6a26db-f129-4bb4-8e39-e4049ca1b4cb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "204f2fe5-c253-4f11-b1d1-5779ca934d5d",
            "compositeImage": {
                "id": "b0307c61-b79a-4a25-b385-ceeaf36685b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e6a26db-f129-4bb4-8e39-e4049ca1b4cb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb0b0f0c-3275-40a2-b33d-58ba5041ac1a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e6a26db-f129-4bb4-8e39-e4049ca1b4cb",
                    "LayerId": "26d8a969-f519-43fe-83b8-0d18980330d8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 520,
    "layers": [
        {
            "id": "26d8a969-f519-43fe-83b8-0d18980330d8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "204f2fe5-c253-4f11-b1d1-5779ca934d5d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 469,
    "yorig": 255
}