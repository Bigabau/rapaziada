{
    "id": "57d6b93a-42bd-4b99-bec7-763c2802f40c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Continue1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 414,
    "bbox_left": 363,
    "bbox_right": 571,
    "bbox_top": 358,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5526ca47-7eb6-410b-a297-41f46eaa1c73",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57d6b93a-42bd-4b99-bec7-763c2802f40c",
            "compositeImage": {
                "id": "43ae9f23-9d46-453e-b3cc-08bbfc06eb23",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5526ca47-7eb6-410b-a297-41f46eaa1c73",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca20b5d8-e26b-48ef-b9d2-62e01d4a25ef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5526ca47-7eb6-410b-a297-41f46eaa1c73",
                    "LayerId": "01f9c95c-df23-4e42-85ee-8aaaa0e8af1c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 520,
    "layers": [
        {
            "id": "01f9c95c-df23-4e42-85ee-8aaaa0e8af1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57d6b93a-42bd-4b99-bec7-763c2802f40c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 0,
    "yorig": 0
}