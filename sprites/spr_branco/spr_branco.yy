{
    "id": "d59846d4-b875-4467-82af-72b0d0da600f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_branco",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4de45f7c-ab44-48c9-92a7-abd04b883377",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d59846d4-b875-4467-82af-72b0d0da600f",
            "compositeImage": {
                "id": "2e314376-7f16-4e6e-a482-66e190af7405",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4de45f7c-ab44-48c9-92a7-abd04b883377",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e204cdb0-9f03-4a58-892c-2759a7eb223f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4de45f7c-ab44-48c9-92a7-abd04b883377",
                    "LayerId": "8dfad934-9b63-4f5f-b335-92e09d8dceca"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8dfad934-9b63-4f5f-b335-92e09d8dceca",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d59846d4-b875-4467-82af-72b0d0da600f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}