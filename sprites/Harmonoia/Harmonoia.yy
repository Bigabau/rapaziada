{
    "id": "1c812ef7-8eab-4035-8fd9-e6b5c883875b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Harmonoia",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 257,
    "bbox_left": 152,
    "bbox_right": 789,
    "bbox_top": 127,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "610723e9-dd00-427a-b7ab-4dea245291a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1c812ef7-8eab-4035-8fd9-e6b5c883875b",
            "compositeImage": {
                "id": "03d6a979-8f93-4721-9651-bc73212dc99e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "610723e9-dd00-427a-b7ab-4dea245291a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ecd502c9-013d-455c-91b6-c09df33229a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "610723e9-dd00-427a-b7ab-4dea245291a4",
                    "LayerId": "5f49d686-c24d-443b-a3a6-1a4fc9e8df6b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 520,
    "layers": [
        {
            "id": "5f49d686-c24d-443b-a3a6-1a4fc9e8df6b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1c812ef7-8eab-4035-8fd9-e6b5c883875b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 484,
    "yorig": 193
}