{
    "id": "71753e59-fc13-4741-81c8-2c4a6d7d1c7d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "QuitGame1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 479,
    "bbox_left": 342,
    "bbox_right": 592,
    "bbox_top": 418,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "34f6eb7c-b69b-455c-938e-def61385e6a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "71753e59-fc13-4741-81c8-2c4a6d7d1c7d",
            "compositeImage": {
                "id": "10f508b9-a68d-4a90-866d-7016f0d5e777",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34f6eb7c-b69b-455c-938e-def61385e6a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6ad5c565-b47e-42a4-b730-3abb230c5a8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34f6eb7c-b69b-455c-938e-def61385e6a1",
                    "LayerId": "b116732f-fe75-4195-82bb-8e7115457baf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 520,
    "layers": [
        {
            "id": "b116732f-fe75-4195-82bb-8e7115457baf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "71753e59-fc13-4741-81c8-2c4a6d7d1c7d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 0,
    "yorig": 0
}