{
    "id": "e86ab307-3fbf-459c-bb9d-b02f81bd7080",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_male_black",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 17,
    "bbox_right": 43,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "19ccbe33-7fe2-4e8b-9259-9d8a2c044c07",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e86ab307-3fbf-459c-bb9d-b02f81bd7080",
            "compositeImage": {
                "id": "3c431d04-4fcd-4278-aea0-aa910449ac18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "19ccbe33-7fe2-4e8b-9259-9d8a2c044c07",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c13ecf60-c66e-4bc2-83e4-32551887928c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "19ccbe33-7fe2-4e8b-9259-9d8a2c044c07",
                    "LayerId": "36de12e6-3ecc-4dd9-b8bc-9ae8dd3a8a25"
                }
            ]
        },
        {
            "id": "7e957069-b380-43d2-b095-c6d38c56461b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e86ab307-3fbf-459c-bb9d-b02f81bd7080",
            "compositeImage": {
                "id": "d33ec4ce-f4e4-45de-9f81-d475990bd82d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e957069-b380-43d2-b095-c6d38c56461b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3239d948-1d75-4780-8751-ad455b8d7e5b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e957069-b380-43d2-b095-c6d38c56461b",
                    "LayerId": "36de12e6-3ecc-4dd9-b8bc-9ae8dd3a8a25"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "36de12e6-3ecc-4dd9-b8bc-9ae8dd3a8a25",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e86ab307-3fbf-459c-bb9d-b02f81bd7080",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 28,
    "yorig": 33
}