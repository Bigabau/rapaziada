{
    "id": "385a9a53-5337-443c-bd73-6eee6bb358ce",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_male_blusa",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 17,
    "bbox_right": 43,
    "bbox_top": 13,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c360d1c0-4790-45a4-aa89-3cf75b9b51fe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "385a9a53-5337-443c-bd73-6eee6bb358ce",
            "compositeImage": {
                "id": "fa319516-4ac3-4548-b00c-bc265b44b9bc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c360d1c0-4790-45a4-aa89-3cf75b9b51fe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b62c82fa-8f9f-4970-8e72-20aae5e05b69",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c360d1c0-4790-45a4-aa89-3cf75b9b51fe",
                    "LayerId": "fdbc5b4d-1c28-4b45-b6d7-0368b997ed7c"
                }
            ]
        },
        {
            "id": "8a409432-c9a6-4f9d-9cce-bda8f7ac456c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "385a9a53-5337-443c-bd73-6eee6bb358ce",
            "compositeImage": {
                "id": "824f76b1-4ff0-4b61-b6ac-348da8d6d045",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a409432-c9a6-4f9d-9cce-bda8f7ac456c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d1d92236-fd9d-431a-9b64-24f3ab85c9a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a409432-c9a6-4f9d-9cce-bda8f7ac456c",
                    "LayerId": "fdbc5b4d-1c28-4b45-b6d7-0368b997ed7c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fdbc5b4d-1c28-4b45-b6d7-0368b997ed7c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "385a9a53-5337-443c-bd73-6eee6bb358ce",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 28,
    "yorig": 33
}