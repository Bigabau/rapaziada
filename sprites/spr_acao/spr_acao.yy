{
    "id": "448997ad-f23b-401f-8395-3fe1098e8122",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_acao",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35fed776-9895-47ab-add9-2ea0d5f21a54",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "448997ad-f23b-401f-8395-3fe1098e8122",
            "compositeImage": {
                "id": "ee1ad00c-e6f8-4012-a285-7ad8d3400c45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35fed776-9895-47ab-add9-2ea0d5f21a54",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7041791b-78f6-4a3e-9fdd-0362a6b8bd90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35fed776-9895-47ab-add9-2ea0d5f21a54",
                    "LayerId": "1236657f-bce4-4720-a21b-dd3afbfc7557"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1236657f-bce4-4720-a21b-dd3afbfc7557",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "448997ad-f23b-401f-8395-3fe1098e8122",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}