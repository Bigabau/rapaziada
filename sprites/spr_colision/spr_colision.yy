{
    "id": "f1b76466-0c0d-4f2d-8b9f-e977be809bb0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_colision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1f74a903-4235-431a-b6a6-82c5275372b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f1b76466-0c0d-4f2d-8b9f-e977be809bb0",
            "compositeImage": {
                "id": "59152443-7e6d-496d-be27-6e4a8955cbf2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f74a903-4235-431a-b6a6-82c5275372b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2c805c6-e695-4bfc-9667-784f603be327",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f74a903-4235-431a-b6a6-82c5275372b1",
                    "LayerId": "bd358b20-f470-4f8d-896e-5ab8220f8154"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bd358b20-f470-4f8d-896e-5ab8220f8154",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f1b76466-0c0d-4f2d-8b9f-e977be809bb0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}