{
    "id": "3f9edfd5-278d-4617-90f4-39214caa5933",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "yan",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 23,
    "bbox_right": 46,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "10caadf8-6b72-45db-8a82-644dd36f8efc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f9edfd5-278d-4617-90f4-39214caa5933",
            "compositeImage": {
                "id": "c86d6820-9e93-4e0d-81d0-decf1d72bc1a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "10caadf8-6b72-45db-8a82-644dd36f8efc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ab08ede1-c507-48a0-b518-3b86da20e5ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "10caadf8-6b72-45db-8a82-644dd36f8efc",
                    "LayerId": "ba701def-d91c-4205-85f0-c5a9a1682d87"
                }
            ]
        },
        {
            "id": "6f7c5a2c-dfcc-4ba3-83d9-537238d73538",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f9edfd5-278d-4617-90f4-39214caa5933",
            "compositeImage": {
                "id": "5842e6ff-3f0a-4811-abb6-0cbb0a027642",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f7c5a2c-dfcc-4ba3-83d9-537238d73538",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "26664f19-ebe7-47ce-a2b3-801b5d9aecd8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f7c5a2c-dfcc-4ba3-83d9-537238d73538",
                    "LayerId": "ba701def-d91c-4205-85f0-c5a9a1682d87"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ba701def-d91c-4205-85f0-c5a9a1682d87",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f9edfd5-278d-4617-90f4-39214caa5933",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 28,
    "yorig": 32
}