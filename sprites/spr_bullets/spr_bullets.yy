{
    "id": "d7c9f119-da3c-4633-80f9-491c4e136819",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bullets",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 6,
    "bbox_left": 1,
    "bbox_right": 6,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bd28b00b-ad6b-47a3-9dfb-a59e2c068972",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7c9f119-da3c-4633-80f9-491c4e136819",
            "compositeImage": {
                "id": "faec873e-099a-46a4-8432-2a15270fdf38",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bd28b00b-ad6b-47a3-9dfb-a59e2c068972",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd77b599-0b9b-4ae9-aa37-b64897ff1c98",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bd28b00b-ad6b-47a3-9dfb-a59e2c068972",
                    "LayerId": "ec7564f5-e9da-4735-b5a6-421c0f86581a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "ec7564f5-e9da-4735-b5a6-421c0f86581a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7c9f119-da3c-4633-80f9-491c4e136819",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}