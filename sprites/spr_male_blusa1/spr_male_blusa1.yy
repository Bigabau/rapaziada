{
    "id": "d0b9d3f4-1dd5-483c-825f-888c05c1d7ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_male_blusa1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 17,
    "bbox_right": 44,
    "bbox_top": 10,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87636e4e-7123-4bec-b166-d38df16ea7f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b9d3f4-1dd5-483c-825f-888c05c1d7ec",
            "compositeImage": {
                "id": "d0f0408e-2bda-40f7-9f01-e30a56173df3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87636e4e-7123-4bec-b166-d38df16ea7f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2d3deb3-ac77-4470-8867-02c744699ff0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87636e4e-7123-4bec-b166-d38df16ea7f5",
                    "LayerId": "6bad343b-4577-4bfe-955d-703acb5bdc19"
                }
            ]
        },
        {
            "id": "3985c944-5f4d-4505-b4a4-409e0b326f7e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d0b9d3f4-1dd5-483c-825f-888c05c1d7ec",
            "compositeImage": {
                "id": "9f8d142d-ef49-492c-98cb-40f47b4044fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3985c944-5f4d-4505-b4a4-409e0b326f7e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cde1ef66-6d52-4d7b-9f1c-45dc295da114",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3985c944-5f4d-4505-b4a4-409e0b326f7e",
                    "LayerId": "6bad343b-4577-4bfe-955d-703acb5bdc19"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6bad343b-4577-4bfe-955d-703acb5bdc19",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d0b9d3f4-1dd5-483c-825f-888c05c1d7ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 28,
    "yorig": 33
}