{
    "id": "235576c7-c72a-4d80-a3f7-b38e46933fc7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sheet_01",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 319,
    "bbox_left": 0,
    "bbox_right": 319,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6ed0f4e9-ba93-48ee-838d-55356ffc23d8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "235576c7-c72a-4d80-a3f7-b38e46933fc7",
            "compositeImage": {
                "id": "7b46c043-51b7-4780-85ad-d4b6016ad9a5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ed0f4e9-ba93-48ee-838d-55356ffc23d8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97d14eea-a277-4c3c-9127-b1862dd2a2ea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ed0f4e9-ba93-48ee-838d-55356ffc23d8",
                    "LayerId": "f77f3515-0023-4bd3-a6cb-f8a0f38c0bae"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "f77f3515-0023-4bd3-a6cb-f8a0f38c0bae",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "235576c7-c72a-4d80-a3f7-b38e46933fc7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}