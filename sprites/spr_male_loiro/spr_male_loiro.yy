{
    "id": "a8625d5c-a66c-4077-8882-5c609c8594c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_male_loiro",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 54,
    "bbox_left": 17,
    "bbox_right": 43,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "477fc56e-e0a4-4d57-a1f8-d838ae88ae78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8625d5c-a66c-4077-8882-5c609c8594c2",
            "compositeImage": {
                "id": "31dc7675-10a8-4eb8-ae34-225b4723df48",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "477fc56e-e0a4-4d57-a1f8-d838ae88ae78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6195cfe9-8ed7-4b6a-852b-c3dacaab9e1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "477fc56e-e0a4-4d57-a1f8-d838ae88ae78",
                    "LayerId": "f4ec722a-8f65-412c-826c-6ecefd59bd8d"
                }
            ]
        },
        {
            "id": "f3d457d6-fc7d-4bdf-8318-6a5576c5e77d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8625d5c-a66c-4077-8882-5c609c8594c2",
            "compositeImage": {
                "id": "28b626e2-8d3d-443c-be54-14ced16fbf01",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3d457d6-fc7d-4bdf-8318-6a5576c5e77d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "121a51e4-2ec0-4f02-bd1e-7bb0c3934b90",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3d457d6-fc7d-4bdf-8318-6a5576c5e77d",
                    "LayerId": "f4ec722a-8f65-412c-826c-6ecefd59bd8d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f4ec722a-8f65-412c-826c-6ecefd59bd8d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8625d5c-a66c-4077-8882-5c609c8594c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 28,
    "yorig": 33
}