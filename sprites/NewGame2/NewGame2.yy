{
    "id": "5d9322d3-4fc7-45a3-be31-90b708b77da1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "NewGame2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 354,
    "bbox_left": 349,
    "bbox_right": 589,
    "bbox_top": 304,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c819d03d-1f23-44df-b061-4ee89f86be01",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d9322d3-4fc7-45a3-be31-90b708b77da1",
            "compositeImage": {
                "id": "d482cd31-5fcb-4f9c-b099-1f1078a3abef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c819d03d-1f23-44df-b061-4ee89f86be01",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fd60acd0-6486-4480-ac15-142d98ffc86d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c819d03d-1f23-44df-b061-4ee89f86be01",
                    "LayerId": "cee1bf53-5121-4479-8d27-75a91980380a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 520,
    "layers": [
        {
            "id": "cee1bf53-5121-4479-8d27-75a91980380a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d9322d3-4fc7-45a3-be31-90b708b77da1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 0,
    "yorig": 0
}