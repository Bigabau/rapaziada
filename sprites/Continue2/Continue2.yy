{
    "id": "5206dcb4-47c3-4407-8c1e-ddab284d84d4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Continue2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 409,
    "bbox_left": 369,
    "bbox_right": 569,
    "bbox_top": 359,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6b6a2e71-4e0a-4241-9de8-373090ced266",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5206dcb4-47c3-4407-8c1e-ddab284d84d4",
            "compositeImage": {
                "id": "9a08b7fe-5241-4c6f-8a33-b6b1e5c716ca",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6b6a2e71-4e0a-4241-9de8-373090ced266",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fe50c89-cb56-4713-8c32-5150db7fe51c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6b6a2e71-4e0a-4241-9de8-373090ced266",
                    "LayerId": "eb9ddd9a-57e8-425e-bdd7-053c68e32c3c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 520,
    "layers": [
        {
            "id": "eb9ddd9a-57e8-425e-bdd7-053c68e32c3c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5206dcb4-47c3-4407-8c1e-ddab284d84d4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 0,
    "yorig": 0
}