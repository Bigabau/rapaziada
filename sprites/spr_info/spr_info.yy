{
    "id": "407c00a6-f41a-4e80-806d-a5b442e12434",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_info",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 319,
    "bbox_left": 18,
    "bbox_right": 319,
    "bbox_top": 18,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9cd9dc9a-b166-4399-874f-acce18392ff2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "407c00a6-f41a-4e80-806d-a5b442e12434",
            "compositeImage": {
                "id": "71d65264-14fa-4b49-9a00-b60d73b719e5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9cd9dc9a-b166-4399-874f-acce18392ff2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9a85a3d-550c-4ffd-a826-8a3bbd5ad130",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9cd9dc9a-b166-4399-874f-acce18392ff2",
                    "LayerId": "c3937b6c-27a7-45b9-8006-a1eee5f6273a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 320,
    "layers": [
        {
            "id": "c3937b6c-27a7-45b9-8006-a1eee5f6273a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "407c00a6-f41a-4e80-806d-a5b442e12434",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 320,
    "xorig": 0,
    "yorig": 0
}