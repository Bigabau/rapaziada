{
    "id": "78268e8e-d4e0-4ab1-9102-d488144fcd4c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_livro",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 20,
    "bbox_right": 42,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9dd24400-ad8f-4e75-89ce-df14da2a1e29",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "78268e8e-d4e0-4ab1-9102-d488144fcd4c",
            "compositeImage": {
                "id": "4d3734d2-16ff-4dd4-9b7b-3842503bf8d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dd24400-ad8f-4e75-89ce-df14da2a1e29",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3f611aa5-5c90-4496-8fb6-0a9affa7d753",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dd24400-ad8f-4e75-89ce-df14da2a1e29",
                    "LayerId": "3023e597-8cee-4df9-995d-4c9b55566c99"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3023e597-8cee-4df9-995d-4c9b55566c99",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "78268e8e-d4e0-4ab1-9102-d488144fcd4c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 29
}