{
    "id": "e92976e7-01b6-4e24-ab83-7e640002f42b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_btn_porta",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 35,
    "bbox_left": 19,
    "bbox_right": 44,
    "bbox_top": 23,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c28c1cab-593d-4d46-b4ea-db3730b7b589",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e92976e7-01b6-4e24-ab83-7e640002f42b",
            "compositeImage": {
                "id": "d5ec0620-fe48-4ebf-8c50-b326247610e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c28c1cab-593d-4d46-b4ea-db3730b7b589",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee6854c8-c317-4260-a748-36cde5e61a96",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c28c1cab-593d-4d46-b4ea-db3730b7b589",
                    "LayerId": "08780842-aa14-44ef-8c00-a36de155d229"
                }
            ]
        },
        {
            "id": "d3941296-607c-44aa-ac5e-f9beba3d5afe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e92976e7-01b6-4e24-ab83-7e640002f42b",
            "compositeImage": {
                "id": "62876286-da7a-432e-8fea-a01f7ff62548",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d3941296-607c-44aa-ac5e-f9beba3d5afe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee07a772-682f-427f-a82d-1a19c375aea2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d3941296-607c-44aa-ac5e-f9beba3d5afe",
                    "LayerId": "08780842-aa14-44ef-8c00-a36de155d229"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "08780842-aa14-44ef-8c00-a36de155d229",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e92976e7-01b6-4e24-ab83-7e640002f42b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}