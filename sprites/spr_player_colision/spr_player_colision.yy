{
    "id": "c15932c4-e69e-4ca1-a53b-46002c25f8ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_colision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 6,
    "bbox_right": 60,
    "bbox_top": 15,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0aad2406-0c84-402c-a9da-dee30174d040",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c15932c4-e69e-4ca1-a53b-46002c25f8ca",
            "compositeImage": {
                "id": "7873b6b9-e11b-4a69-88d7-b9e5179bacfd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0aad2406-0c84-402c-a9da-dee30174d040",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a86444f-fc41-4aa1-941d-5c2e70b9e77b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0aad2406-0c84-402c-a9da-dee30174d040",
                    "LayerId": "92ab6759-4c25-44a8-b5ce-5a09169ccf58"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "92ab6759-4c25-44a8-b5ce-5a09169ccf58",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c15932c4-e69e-4ca1-a53b-46002c25f8ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 28,
    "yorig": 33
}