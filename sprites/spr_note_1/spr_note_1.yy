{
    "id": "c8a74895-957f-4e78-9ea0-eadf4f1cc6db",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_note_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 18,
    "bbox_right": 57,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "537f5bd6-a9df-42cd-bdd2-7c5c4abecd3d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c8a74895-957f-4e78-9ea0-eadf4f1cc6db",
            "compositeImage": {
                "id": "b58ecc44-b40f-4186-9af6-dd70586fadb8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "537f5bd6-a9df-42cd-bdd2-7c5c4abecd3d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4378fade-8e01-408e-99b0-97f19be11c35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "537f5bd6-a9df-42cd-bdd2-7c5c4abecd3d",
                    "LayerId": "fdfe05f0-78d9-430f-b971-03de90a4cbdb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fdfe05f0-78d9-430f-b971-03de90a4cbdb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c8a74895-957f-4e78-9ea0-eadf4f1cc6db",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 21
}