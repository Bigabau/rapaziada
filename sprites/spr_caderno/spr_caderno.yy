{
    "id": "602c7731-7bf2-4c46-81dc-aa7cc5fdec7f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_caderno",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 43,
    "bbox_left": 25,
    "bbox_right": 37,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "32905d40-bfe9-4748-a1aa-cf24c2bb851c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "602c7731-7bf2-4c46-81dc-aa7cc5fdec7f",
            "compositeImage": {
                "id": "8fc7ca7e-e5b7-4407-a150-abbcb1feef5e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "32905d40-bfe9-4748-a1aa-cf24c2bb851c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c59f47f3-7e86-40c1-9a0f-ad9db7f87392",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "32905d40-bfe9-4748-a1aa-cf24c2bb851c",
                    "LayerId": "68e361e3-221d-40dd-a9b7-f4097a85a131"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "68e361e3-221d-40dd-a9b7-f4097a85a131",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "602c7731-7bf2-4c46-81dc-aa7cc5fdec7f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 32,
    "yorig": 32
}