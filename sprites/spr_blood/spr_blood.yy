{
    "id": "c4e9599e-b2b1-4c89-9635-cc17c019a268",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_blood",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 105,
    "bbox_left": 22,
    "bbox_right": 105,
    "bbox_top": 44,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a57fbc58-9ac7-4242-a5b5-5d7bc8109775",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4e9599e-b2b1-4c89-9635-cc17c019a268",
            "compositeImage": {
                "id": "e5a7436c-62ad-46f4-81c1-7d0adb5ac927",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a57fbc58-9ac7-4242-a5b5-5d7bc8109775",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cfa7181f-275b-4960-8649-1e235ce2f50b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a57fbc58-9ac7-4242-a5b5-5d7bc8109775",
                    "LayerId": "2be98c84-f0c7-46ed-add1-e0098ae7a267"
                }
            ]
        },
        {
            "id": "0e0935a4-a56b-44ff-a136-3e1183d211b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4e9599e-b2b1-4c89-9635-cc17c019a268",
            "compositeImage": {
                "id": "bcd17a60-62af-47c4-9acc-de1c00dd5e14",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0e0935a4-a56b-44ff-a136-3e1183d211b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd2ed819-9aaf-4037-995c-fa8eeaa0a475",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0e0935a4-a56b-44ff-a136-3e1183d211b0",
                    "LayerId": "2be98c84-f0c7-46ed-add1-e0098ae7a267"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "2be98c84-f0c7-46ed-add1-e0098ae7a267",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4e9599e-b2b1-4c89-9635-cc17c019a268",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 64
}