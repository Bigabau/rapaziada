{
    "id": "236cefce-d383-4dd9-ae36-b606261a7ceb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_cadeira",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 36,
    "bbox_left": 17,
    "bbox_right": 43,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "81bb2fab-a7a0-480c-b16c-e2aea5fb97be",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "236cefce-d383-4dd9-ae36-b606261a7ceb",
            "compositeImage": {
                "id": "edebc09a-35ee-499f-8561-a203eb89785e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81bb2fab-a7a0-480c-b16c-e2aea5fb97be",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4dea4e71-767a-458c-ba14-c484296811b8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81bb2fab-a7a0-480c-b16c-e2aea5fb97be",
                    "LayerId": "01ae2b86-75d1-4172-8f21-43cd370f0b16"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "01ae2b86-75d1-4172-8f21-43cd370f0b16",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "236cefce-d383-4dd9-ae36-b606261a7ceb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 29,
    "yorig": 23
}