{
    "id": "bf411663-3a7f-4d7f-846e-7fc84b488c43",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "QuitGame2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 478,
    "bbox_left": 348,
    "bbox_right": 590,
    "bbox_top": 423,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a999574a-1aee-4238-b8bc-2751b96c9a5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bf411663-3a7f-4d7f-846e-7fc84b488c43",
            "compositeImage": {
                "id": "4fcce2af-f438-40bf-8448-a3361bef8ede",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a999574a-1aee-4238-b8bc-2751b96c9a5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "86e13782-5ebc-4ccc-bf22-f65ae359869a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a999574a-1aee-4238-b8bc-2751b96c9a5a",
                    "LayerId": "d30cb300-8b4f-40fb-97f9-138ac8f5d817"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 520,
    "layers": [
        {
            "id": "d30cb300-8b4f-40fb-97f9-138ac8f5d817",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bf411663-3a7f-4d7f-846e-7fc84b488c43",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 0,
    "yorig": 0
}