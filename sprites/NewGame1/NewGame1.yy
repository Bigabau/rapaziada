{
    "id": "983be562-6dff-4f0f-846f-52ff6dfec7de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "NewGame1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 355,
    "bbox_left": 343,
    "bbox_right": 591,
    "bbox_top": 299,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "52ee27e8-c5ba-49d9-8c85-03f897f40e8e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "983be562-6dff-4f0f-846f-52ff6dfec7de",
            "compositeImage": {
                "id": "7b3905ce-5c0d-4dd0-aafb-64963487e440",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "52ee27e8-c5ba-49d9-8c85-03f897f40e8e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e66c424d-a0fa-4175-95ef-7c2734c4ba15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "52ee27e8-c5ba-49d9-8c85-03f897f40e8e",
                    "LayerId": "d803016a-bff4-4887-bb23-9aff982aaf22"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 520,
    "layers": [
        {
            "id": "d803016a-bff4-4887-bb23-9aff982aaf22",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "983be562-6dff-4f0f-846f-52ff6dfec7de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 900,
    "xorig": 0,
    "yorig": 0
}