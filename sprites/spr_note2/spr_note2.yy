{
    "id": "abdc2a51-ea6b-41ee-a2c3-fe212056d42a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_note2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 37,
    "bbox_left": 18,
    "bbox_right": 59,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "87bf529c-028e-4107-9cf3-94c02610a9d7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "abdc2a51-ea6b-41ee-a2c3-fe212056d42a",
            "compositeImage": {
                "id": "d30be516-1516-43e3-bbf3-b44a48f88536",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "87bf529c-028e-4107-9cf3-94c02610a9d7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84996a25-79fb-43d5-91f3-dc079c40b7eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "87bf529c-028e-4107-9cf3-94c02610a9d7",
                    "LayerId": "643ed7f7-bfb2-4732-af8d-802fe41c1112"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "643ed7f7-bfb2-4732-af8d-802fe41c1112",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "abdc2a51-ea6b-41ee-a2c3-fe212056d42a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 31,
    "yorig": 21
}