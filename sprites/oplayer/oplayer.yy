{
    "id": "e17b12f7-4b51-41f4-b67e-2e461cedb654",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "oplayer",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 23,
    "bbox_right": 46,
    "bbox_top": 22,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "584b2846-f6f8-4f5e-ad16-f1b4e5e432f1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e17b12f7-4b51-41f4-b67e-2e461cedb654",
            "compositeImage": {
                "id": "a4fa0334-b9c1-40ad-bd15-f5cb3c27f586",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "584b2846-f6f8-4f5e-ad16-f1b4e5e432f1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca94c9c7-61d8-44b9-a20a-8a16ed1d6619",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "584b2846-f6f8-4f5e-ad16-f1b4e5e432f1",
                    "LayerId": "aa1d565d-c9bd-49a7-8dbc-5607090b8201"
                }
            ]
        },
        {
            "id": "874d0af4-51e5-4119-a64a-c442081e7180",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e17b12f7-4b51-41f4-b67e-2e461cedb654",
            "compositeImage": {
                "id": "605fb33e-c8bc-4b3c-9dab-14b5faac54ae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "874d0af4-51e5-4119-a64a-c442081e7180",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aba4c9b8-b3ce-4d74-a7a5-336513694f08",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "874d0af4-51e5-4119-a64a-c442081e7180",
                    "LayerId": "aa1d565d-c9bd-49a7-8dbc-5607090b8201"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "aa1d565d-c9bd-49a7-8dbc-5607090b8201",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e17b12f7-4b51-41f4-b67e-2e461cedb654",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": true,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 26,
    "yorig": 32
}