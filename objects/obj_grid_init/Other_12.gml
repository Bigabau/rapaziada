while (true) {
	var x_cell = irandom_range(0, global.grid_width)
	var y_cell = irandom_range(0, global.grid_height)
	
	var position_x = (x_cell * global.cellsize);
	var position_y = (y_cell * global.cellsize);
	
	if (!place_meeting(position_x, position_y, obj_Wall)) {
		instance_create_depth(position_x, position_y, depth, obj_Entity);
		break;
	}
}