{
    "id": "cb2415c0-f7fe-4ae1-bd17-512f3447bdcb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_grid_init",
    "eventList": [
        {
            "id": "1b7f1f01-c07b-4495-a2af-cb3fdbcf1793",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "cb2415c0-f7fe-4ae1-bd17-512f3447bdcb"
        },
        {
            "id": "ac9bf2b9-d65a-4985-b643-dc23126ab01e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "cb2415c0-f7fe-4ae1-bd17-512f3447bdcb"
        },
        {
            "id": "d225dce2-9fdd-40d6-88b7-85678c9f2869",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "cb2415c0-f7fe-4ae1-bd17-512f3447bdcb"
        },
        {
            "id": "12161dbb-c6c7-4a97-b7b3-53bb1c3cd707",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "cb2415c0-f7fe-4ae1-bd17-512f3447bdcb"
        },
        {
            "id": "e62f39a3-e8d8-4fba-9e72-84d4cba74586",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 12,
            "eventtype": 7,
            "m_owner": "cb2415c0-f7fe-4ae1-bd17-512f3447bdcb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}