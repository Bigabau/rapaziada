{
    "id": "70aef4d9-e56c-4fdd-aebb-f72ff09349ba",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_newgame_01",
    "eventList": [
        {
            "id": "3c4f4c94-ba0a-409b-a374-3221823ca1ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "70aef4d9-e56c-4fdd-aebb-f72ff09349ba"
        },
        {
            "id": "6db03192-8884-4cef-87c4-c1918c66e023",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "70aef4d9-e56c-4fdd-aebb-f72ff09349ba"
        },
        {
            "id": "77b3c562-8891-4950-bbf3-7cb7e1607450",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "70aef4d9-e56c-4fdd-aebb-f72ff09349ba"
        },
        {
            "id": "e03407e0-e4e0-49b9-8fb8-defe906d9d78",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "70aef4d9-e56c-4fdd-aebb-f72ff09349ba"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "983be562-6dff-4f0f-846f-52ff6dfec7de",
    "visible": true
}