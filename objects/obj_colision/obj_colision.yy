{
    "id": "9f604f89-4e6d-44ff-9436-4bd7e1f9e3b9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_colision",
    "eventList": [
        {
            "id": "d9e78f43-0aec-4cc6-b785-2fea0fb42ee0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9f604f89-4e6d-44ff-9436-4bd7e1f9e3b9"
        },
        {
            "id": "0d20baea-15b2-4594-8c3c-ec131d47674b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9f604f89-4e6d-44ff-9436-4bd7e1f9e3b9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "659c1ed8-6be9-4c9b-9a9a-5d0762ca1f13",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "f1b76466-0c0d-4f2d-8b9f-e977be809bb0",
    "visible": false
}