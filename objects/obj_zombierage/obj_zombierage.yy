{
    "id": "fcef5980-72a7-4d3e-8273-e0a6ce56877b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_zombierage",
    "eventList": [
        {
            "id": "02700466-e978-4a3f-8e26-38979c56253f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fcef5980-72a7-4d3e-8273-e0a6ce56877b"
        },
        {
            "id": "4c236892-68f6-4b28-8f11-a025729979b5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fcef5980-72a7-4d3e-8273-e0a6ce56877b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "204f2fe5-c253-4f11-b1d1-5779ca934d5d",
    "visible": true
}