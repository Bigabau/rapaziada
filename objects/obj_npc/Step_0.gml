randomize();

if(dead == true){
depth = 2;	
}

#region //Reseta
if(keyboard_check(ord("R"))){
	room_persistent = false;
	room_goto(rm_central)
}


#endregion

#region Check morto e Colisao com bala
if(dead = false){
	image_angle = direction;
}

if(image_index == 0){
	var bala = instance_place(x,y,obj_bullets);
	if(bala){
		dead = true;
		image_angle = irandom_range(0,360)
		image_index = 1;
		var blood = instance_create_depth(x,y,+5,obj_blood)
		
		with(blood){
		image_xscale = other.alt2
		image_yscale = other.alt2
		}
		
		with(bala){
			instance_destroy()
		}
	}
}
#endregion


#region //Seguir player

DidHit = collision_circle(x, y, 130, obj_player, false, true)
	 
if (DidHit) {	 
	Target = DidHit
}

if (Target) {
	mp_potential_step(Target.x,Target.y,1.5,0)
	image_angle = point_direction(x,y,Target.x,Target.y)	
}





#endregion
