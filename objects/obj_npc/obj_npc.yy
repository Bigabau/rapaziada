{
    "id": "6acf4305-06d4-4782-84e5-27c73bb7707a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_npc",
    "eventList": [
        {
            "id": "b5feca25-a1ed-458d-877a-9fec556c57e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "6acf4305-06d4-4782-84e5-27c73bb7707a"
        },
        {
            "id": "2e38ce4d-e54f-49ee-8331-6c0e33d83975",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "6acf4305-06d4-4782-84e5-27c73bb7707a"
        },
        {
            "id": "94314984-7eed-42a6-a2f9-72ae0eb5ac6c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6acf4305-06d4-4782-84e5-27c73bb7707a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6acf4305-06d4-4782-84e5-27c73bb7707a"
        },
        {
            "id": "16dec981-b300-44ef-969f-d58b46e29fe8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "29da050a-e526-46e9-bd4f-33a0eb7b7c95",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6acf4305-06d4-4782-84e5-27c73bb7707a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "a8625d5c-a66c-4077-8882-5c609c8594c2",
    "visible": true
}