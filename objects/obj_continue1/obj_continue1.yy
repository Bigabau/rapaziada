{
    "id": "ea2d953d-d3ee-4dd0-97df-7d97a2cf25da",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_continue1",
    "eventList": [
        {
            "id": "ad6ea9af-faa7-4f3c-b1e6-47eab8b7f3d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ea2d953d-d3ee-4dd0-97df-7d97a2cf25da"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "57d6b93a-42bd-4b99-bec7-763c2802f40c",
    "visible": true
}