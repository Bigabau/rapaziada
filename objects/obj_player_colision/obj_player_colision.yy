{
    "id": "13210515-362b-4c6b-a7fb-9857345caad0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_colision",
    "eventList": [
        {
            "id": "781b87d9-0f5b-406d-b243-4c3ca3da36be",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "13210515-362b-4c6b-a7fb-9857345caad0"
        },
        {
            "id": "7721ed6e-eb46-4d36-961b-aed8d21f0b1d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "13210515-362b-4c6b-a7fb-9857345caad0"
        },
        {
            "id": "415d2c86-5561-439c-b212-268615a81be2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "9f604f89-4e6d-44ff-9436-4bd7e1f9e3b9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "13210515-362b-4c6b-a7fb-9857345caad0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c15932c4-e69e-4ca1-a53b-46002c25f8ca",
    "visible": true
}