{
    "id": "3681ec4e-d207-43df-ad8b-56aa0015100b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_blood",
    "eventList": [
        {
            "id": "c361f8bd-1461-4516-ba21-ae6ea21999cd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3681ec4e-d207-43df-ad8b-56aa0015100b"
        },
        {
            "id": "9b1a4fee-4602-453d-a149-38328fe52592",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3681ec4e-d207-43df-ad8b-56aa0015100b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c4e9599e-b2b1-4c89-9635-cc17c019a268",
    "visible": true
}