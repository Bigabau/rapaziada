// oque ee

if(keyboard_check_pressed(vk_enter)){
	with(obj_npc){
		instance_destroy();
	}
}

if(room == rm_central){
	room_persistent = true;
} else if(room == rm_sala_informatica){
	room_persistent = true;
}
 
 
 depth = -y;

if(PosIDD == rm_central){
	x = 480;
	y = 568;
	PosIDD = -1;
}

if(mouse_check_button(mb_right)){
	instance_create_depth(mouse_x,mouse_y,-10,obj_blood)
}

if(PosIDD == rm_sala_informatica){
	x = 480;
	y = 46;
	PosIDD = -1;
}


#region //Ação Room


#endregion

#region // Movimento

#endregion

#region // Player2

if (Index <= 1) {
	for (var i = 0; i < gamepad_get_device_count(); i++;) {
	   if gamepad_is_connected(i) && !Gamepads[i] {
			if (gamepad_button_check_pressed(i, gp_start)) {
				show_debug_message("2Players")
				
				Gamepads[i] = true;
				
				var NewCharacter = instance_create_depth(x, y, 0, obj_player);
				NewCharacter.image_xscale = image_xscale;
				NewCharacter.image_yscale = image_yscale;
				NewCharacter.CurrentGamepad = i
			}
	   }
	}
}

#endregion

#region // Direction

if (Index <= 1) {
	image_angle = point_direction(x,y,mouse_x,mouse_y);
	direction = image_angle;
} else if (Index >= 2) {
	moveX = gamepad_axis_value(CurrentGamepad, gp_axislh);
	moveY = gamepad_axis_value(CurrentGamepad, gp_axislv);
	moveXM = gamepad_axis_value(CurrentGamepad, gp_axisrh);
	moveYM = gamepad_axis_value(CurrentGamepad, gp_axisrv);	
	
	MH = moveXM * spd;
	MV = moveYM *spd;
	
	if(moveXM <> 0 ){
		image_angle = point_direction(x, y, x + MH, y + MV)
	}
	if(moveYM <> 0){
		image_angle = point_direction(x, y, x + MH, y + MV)
	}	
}

//show_debug_message(string(point_direction(x,y,mouse_x,mouse_y)));

#endregion


#region //Luz
/*
polygon = polygon_from_instance(id);
*/
#endregion 

#region //Controles
x = Colisao.x
y = Colisao.y


#endregion 

#region //Camêra


camX += (x - (view_wport[0]/3.3) - camX)*0.05  //Smoth CameraX
camY += (y - (view_hport[0]/3.3) - camY)*0.05  //Smoth CameraY


var xxx = clamp(camX,16,375); //Limitador de Valor no Caso CameraX
var yyy = clamp(camY,16,160); //Limitador de Valor no Caso CameraY



camera_set_view_pos(view_camera[0],xxx,yyy) //Move a Camera

#endregion

#region //Colisão

#endregion

#region //Image



if(Index <= 1 and keyboard_check_pressed(ord("Q")) 
or Index >= 2 and gamepad_button_check_pressed(CurrentGamepad,gp_shoulderlb)){
	ArmaEquipada = !ArmaEquipada;
}

if(ArmaEquipada = true){
	image_index = 1;
} else {
	image_index = 0;
}

if (Index >= 2 and gamepad_button_check_pressed(CurrentGamepad,gp_shoulderrb) && ArmaEquipada = true) {
	var bala = instance_create_depth(x+16,y+6,-10,obj_bullets)
	with(bala){
		direction = other.image_angle;
	}
}

if((Index <= 1 and mouse_check_button_pressed(mb_left)) && ArmaEquipada = true) {
	show_debug_message("aaaaaaa")
	if(direction > 0 && direction < 91){
		var bala = instance_create_depth((x+16)+lengthdir_x(16,image_angle),(y+16)+lengthdir_y(16,image_angle) ,-5,obj_bullets)
		with(bala){
		direction = point_direction(x,y,mouse_x,mouse_y);
		}
	}
	else if(direction > 91 && direction < 181){
		var bala = instance_create_depth((x+6)+lengthdir_x(16,image_angle),(y-24)+lengthdir_y(24,image_angle) ,-5,obj_bullets)
		with(bala){
		direction = point_direction(x,y,mouse_x,mouse_y);
		}
	}

	else if(direction > 181 && direction < 271){
		var bala = instance_create_depth((x-24)+lengthdir_x(16,image_angle),(y-16)+lengthdir_y(16,image_angle) ,-5,obj_bullets)
		with(bala){
			direction = point_direction(x,y,mouse_x,mouse_y);
		}
	}

	else if(image_angle > 270){
		var bala = instance_create_depth((x-16)+lengthdir_x(16,image_angle),(y+16)+lengthdir_y(16,image_angle) ,-5,obj_bullets)
		with(bala){
			direction = point_direction(x,y,mouse_x,mouse_y);
		}
	}

}


#endregion
