{
    "id": "f8799b61-a241-4460-8e4a-0777c8847fb0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "cb44eed9-1503-49a4-a581-4b0591d51fc2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f8799b61-a241-4460-8e4a-0777c8847fb0"
        },
        {
            "id": "fd1edc8a-9cb4-4241-8b38-a352b6aa93f4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f8799b61-a241-4460-8e4a-0777c8847fb0"
        },
        {
            "id": "9ec86d45-c32d-456c-b4f5-e9e456c2226c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 60,
            "eventtype": 6,
            "m_owner": "f8799b61-a241-4460-8e4a-0777c8847fb0"
        },
        {
            "id": "3c66ff7f-f785-4849-8567-e81f2aa3dbbc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "f8799b61-a241-4460-8e4a-0777c8847fb0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5b382bb6-e44d-484e-b997-95759822cc3e",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5104bf9b-0d37-4181-8c94-73850e3cf6cd",
    "visible": true
}