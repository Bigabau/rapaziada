{
    "id": "5bf9f02f-c19b-43b1-af06-97dffc4c4707",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_note",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "42c81e2b-f04b-4b1d-ab00-a05bbcfe582e",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c8a74895-957f-4e78-9ea0-eadf4f1cc6db",
    "visible": true
}