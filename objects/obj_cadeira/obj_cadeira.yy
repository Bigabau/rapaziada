{
    "id": "42c81e2b-f04b-4b1d-ab00-a05bbcfe582e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_cadeira",
    "eventList": [
        {
            "id": "51171eb9-88b9-42bf-ad25-5d186c96645b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "42c81e2b-f04b-4b1d-ab00-a05bbcfe582e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "236cefce-d383-4dd9-ae36-b606261a7ceb",
    "visible": true
}