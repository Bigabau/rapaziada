{
    "id": "e9c37c4b-3a1c-4e7c-8ab2-2624d559082f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_bullets",
    "eventList": [
        {
            "id": "129bcfe2-cc10-4db7-be52-430e567c248c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "e9c37c4b-3a1c-4e7c-8ab2-2624d559082f"
        },
        {
            "id": "544965eb-f4de-47a3-a8a5-1eb593c02bb1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "e9c37c4b-3a1c-4e7c-8ab2-2624d559082f"
        },
        {
            "id": "5d0cd85a-845a-425c-a4bb-397ebf0e3438",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "e9c37c4b-3a1c-4e7c-8ab2-2624d559082f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d7c9f119-da3c-4633-80f9-491c4e136819",
    "visible": true
}