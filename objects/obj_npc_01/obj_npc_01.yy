{
    "id": "29da050a-e526-46e9-bd4f-33a0eb7b7c95",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_npc_01",
    "eventList": [
        {
            "id": "d097a9a7-18d9-4d0e-9ab5-51348743c70f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "29da050a-e526-46e9-bd4f-33a0eb7b7c95"
        },
        {
            "id": "edd8c46b-642d-45fa-b523-aad3589078d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "29da050a-e526-46e9-bd4f-33a0eb7b7c95"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "6acf4305-06d4-4782-84e5-27c73bb7707a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "a8625d5c-a66c-4077-8882-5c609c8594c2",
    "visible": true
}