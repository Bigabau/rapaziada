{
    "id": "8ad391eb-2bf1-4308-8981-78e5af036897",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_harmonia",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "fcef5980-72a7-4d3e-8273-e0a6ce56877b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1c812ef7-8eab-4035-8fd9-e6b5c883875b",
    "visible": true
}