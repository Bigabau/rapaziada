{
    "id": "37f15e6c-5818-49f6-bdbd-fcc57b999c69",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_branco",
    "eventList": [
        {
            "id": "e920b31b-3cf0-4e07-b8fa-fa98e5cdbe55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "37f15e6c-5818-49f6-bdbd-fcc57b999c69"
        },
        {
            "id": "33f99e04-56d1-4880-a63d-4f223c23945c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "37f15e6c-5818-49f6-bdbd-fcc57b999c69"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d59846d4-b875-4467-82af-72b0d0da600f",
    "visible": true
}