{
    "id": "a77df899-5384-4b0c-bfe2-476879982baa",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_box_acao",
    "eventList": [
        {
            "id": "0bc637b3-2bd9-42b5-899f-627d27696e4e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a77df899-5384-4b0c-bfe2-476879982baa"
        },
        {
            "id": "1a83f1a8-5681-4224-bb87-4808bd09e824",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a77df899-5384-4b0c-bfe2-476879982baa"
        },
        {
            "id": "8ebdaa56-2d67-4c01-bb6d-82944393e7a0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a77df899-5384-4b0c-bfe2-476879982baa"
        },
        {
            "id": "bde44f22-bb12-4ab8-b865-57331e335f63",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "a77df899-5384-4b0c-bfe2-476879982baa"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "448997ad-f23b-401f-8395-3fe1098e8122",
    "visible": true
}