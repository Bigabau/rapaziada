{
    "id": "040b2a2a-b9d6-4f10-8394-359b39ed61c9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_continue2",
    "eventList": [
        {
            "id": "793ce554-e49f-48e6-8544-a7a2de72cfb9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "040b2a2a-b9d6-4f10-8394-359b39ed61c9"
        },
        {
            "id": "534f668c-fa98-40f0-bfef-5ac173e6daa2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "040b2a2a-b9d6-4f10-8394-359b39ed61c9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0369b2fb-6a10-463f-9041-a7cf78c61a01",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5206dcb4-47c3-4407-8c1e-ddab284d84d4",
    "visible": true
}