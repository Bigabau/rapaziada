{
    "id": "def791ef-93ad-4f68-9556-6e8cddc41059",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_quitgame2",
    "eventList": [
        {
            "id": "df09de67-00fd-4e4d-ada1-ca0b1c0f58ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "def791ef-93ad-4f68-9556-6e8cddc41059"
        },
        {
            "id": "55bd3397-185f-48a3-a88c-b1088809cf68",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "def791ef-93ad-4f68-9556-6e8cddc41059"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "0369b2fb-6a10-463f-9041-a7cf78c61a01",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bf411663-3a7f-4d7f-846e-7fc84b488c43",
    "visible": true
}