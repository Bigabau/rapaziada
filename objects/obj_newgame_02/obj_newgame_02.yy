{
    "id": "0369b2fb-6a10-463f-9041-a7cf78c61a01",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_newgame_02",
    "eventList": [
        {
            "id": "52b1badd-aec0-41fc-ad4a-c4f4714a8257",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0369b2fb-6a10-463f-9041-a7cf78c61a01"
        },
        {
            "id": "7419c9e2-2015-4cdd-a53b-2859f4df7f16",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0369b2fb-6a10-463f-9041-a7cf78c61a01"
        },
        {
            "id": "6cb7f4f5-d2fb-4c2b-86c0-cf6c5cbdb88a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "0369b2fb-6a10-463f-9041-a7cf78c61a01"
        },
        {
            "id": "ee9e2f6a-d28d-4743-abba-65b7422362e6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "0369b2fb-6a10-463f-9041-a7cf78c61a01"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "5d9322d3-4fc7-45a3-be31-90b708b77da1",
    "visible": true
}