{
    "id": "d585ab58-6f3d-4598-85f8-930ad2f910ef",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_ui",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "7ff03095-0ef8-4806-afa9-1ed5bd04f679",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 55,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "e440ed2e-51d1-4e44-a1ca-02979ff13146",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 55,
                "offset": 4,
                "shift": 13,
                "w": 6,
                "x": 112,
                "y": 173
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "1b59e4c6-e916-473c-be4c-89f0a9c1fe40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 55,
                "offset": 2,
                "shift": 17,
                "w": 13,
                "x": 97,
                "y": 173
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "a0844614-2dde-4360-b013-2b2d9d2a1831",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 55,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 68,
                "y": 173
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e001acb2-1843-48dc-ae66-88fe8bb8dc32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 55,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 42,
                "y": 173
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "efd893eb-c316-43ad-bce3-148b522a4a66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 55,
                "offset": 2,
                "shift": 43,
                "w": 38,
                "x": 2,
                "y": 173
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "f3569ea3-a908-43a9-bc52-38586c6b414c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 55,
                "offset": 2,
                "shift": 32,
                "w": 29,
                "x": 455,
                "y": 116
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4daf4e42-72ef-4bc0-9c5a-f5b547c65943",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 55,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 448,
                "y": 116
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "cf54abe3-7202-4a20-accd-b8762ad39a30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 55,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 433,
                "y": 116
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "fb2fd798-3f5a-4587-80a5-8c34fcd47b0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 55,
                "offset": 2,
                "shift": 16,
                "w": 13,
                "x": 418,
                "y": 116
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "2611db27-9b02-49f5-adc3-6688de2599d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 55,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 120,
                "y": 173
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "fe1aec61-e3df-4608-b33a-69851684973e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 392,
                "y": 116
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "72220996-2c18-4b12-a2ca-fd4dfc830680",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 55,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 358,
                "y": 116
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "539f04d9-d4cb-4764-8f24-0c07257896a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 55,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 342,
                "y": 116
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5a22bd1c-68a3-49e4-9965-97059b2a7060",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 55,
                "offset": 4,
                "shift": 13,
                "w": 6,
                "x": 334,
                "y": 116
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "4bbd1067-e439-4c12-8c04-036df65f3f7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 55,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 318,
                "y": 116
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "d84b766b-88b9-4866-a6eb-adb2f9206dd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 55,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 293,
                "y": 116
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "c22fe884-5529-483b-a080-4abf198c7176",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 55,
                "offset": 5,
                "shift": 27,
                "w": 13,
                "x": 278,
                "y": 116
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "97411d54-c7c8-4d46-b574-3218f6aef06d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 55,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 252,
                "y": 116
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "fcdc75c3-62d7-4d1d-8d83-ebec6b7028c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 55,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 227,
                "y": 116
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "bcd526a4-064b-4903-957d-cdd33c5f3c5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 55,
                "offset": 0,
                "shift": 27,
                "w": 25,
                "x": 200,
                "y": 116
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "60c545ec-d1ff-4072-829a-4e5d04874bac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 55,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 367,
                "y": 116
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "17391f4b-884c-45d9-bfc1-634e12d7fb9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 55,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 138,
                "y": 173
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "af631059-3da7-4a19-9618-c773eb4cceb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 55,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 164,
                "y": 173
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "2154a704-7d5d-4efc-a066-d6145f8427dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 55,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 189,
                "y": 173
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "33eee2ef-f5b5-4bf5-ba44-9cba2e8223aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 55,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 267,
                "y": 230
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "b6d9beeb-90d2-4a16-a420-970db6421b27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 55,
                "offset": 4,
                "shift": 13,
                "w": 6,
                "x": 259,
                "y": 230
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "fcf6e83a-52e6-4945-a4f6-03c2898f39c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 55,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 250,
                "y": 230
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e3dae395-eaf7-4710-9512-12fbdd088a52",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 224,
                "y": 230
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "9e3cf961-0a80-4c61-afa2-29fbd436c876",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 198,
                "y": 230
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "1c3002ba-e0b7-4fd1-9652-cf3bd03db451",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 172,
                "y": 230
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "bc420dcc-f6ad-47a6-b234-a12f1a57ce07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 55,
                "offset": 2,
                "shift": 27,
                "w": 23,
                "x": 147,
                "y": 230
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c44192e6-d42a-4d0f-9acf-098de7b362c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 55,
                "offset": 2,
                "shift": 49,
                "w": 45,
                "x": 100,
                "y": 230
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "56ef5af2-20b2-4bbe-a0a9-2140b2f8c7ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 55,
                "offset": -1,
                "shift": 32,
                "w": 34,
                "x": 64,
                "y": 230
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "3b3b1af6-38a4-42a4-acca-28f9fd118242",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 55,
                "offset": 3,
                "shift": 32,
                "w": 27,
                "x": 35,
                "y": 230
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "c7c4e948-5997-414d-a82d-6f8fbd58a685",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 55,
                "offset": 2,
                "shift": 35,
                "w": 31,
                "x": 2,
                "y": 230
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "9001797b-1bfe-4c42-89bd-0798a7fa3a3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 55,
                "offset": 3,
                "shift": 35,
                "w": 30,
                "x": 457,
                "y": 173
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "980b16d6-ac62-4c6e-9c87-36f6e3fbdb68",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 55,
                "offset": 3,
                "shift": 32,
                "w": 27,
                "x": 428,
                "y": 173
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "2e18f346-ed26-4c08-ab2d-c4bf88eaa0d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 55,
                "offset": 3,
                "shift": 29,
                "w": 25,
                "x": 401,
                "y": 173
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "7454809d-7fad-45f1-8d02-2ad7df9f0d4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 55,
                "offset": 2,
                "shift": 37,
                "w": 33,
                "x": 366,
                "y": 173
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "4738d97e-905c-437a-a0f0-bb26b66e26cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 55,
                "offset": 3,
                "shift": 35,
                "w": 28,
                "x": 336,
                "y": 173
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "84248236-a1da-4a8b-8a99-bcda806b3f2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 55,
                "offset": 4,
                "shift": 13,
                "w": 6,
                "x": 328,
                "y": 173
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "57f02fb0-0eff-4c60-bb23-3ab917f69d36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 55,
                "offset": 1,
                "shift": 24,
                "w": 20,
                "x": 306,
                "y": 173
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "683985c8-e979-4189-883b-6e0706052865",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 55,
                "offset": 3,
                "shift": 32,
                "w": 29,
                "x": 275,
                "y": 173
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "3ae0889b-7126-463f-aa4e-c70449697ba8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 55,
                "offset": 3,
                "shift": 27,
                "w": 22,
                "x": 251,
                "y": 173
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "c4081459-c551-47e0-b13a-be6cb41b856f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 55,
                "offset": 3,
                "shift": 40,
                "w": 34,
                "x": 215,
                "y": 173
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "c2e5e767-20fe-4e49-862b-a604b645037b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 55,
                "offset": 3,
                "shift": 35,
                "w": 28,
                "x": 170,
                "y": 116
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "bdf54410-68b0-4e4e-9116-d91668c6c84b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 55,
                "offset": 2,
                "shift": 37,
                "w": 34,
                "x": 134,
                "y": 116
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b87047a5-e1e9-4016-8613-6b2c9ee1e260",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 55,
                "offset": 3,
                "shift": 32,
                "w": 27,
                "x": 105,
                "y": 116
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f38d181f-7c03-43d6-a88b-df609cdde613",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 55,
                "offset": 2,
                "shift": 37,
                "w": 34,
                "x": 78,
                "y": 59
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "1c8e9631-7904-4d22-b48b-d15333c79e9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 55,
                "offset": 3,
                "shift": 35,
                "w": 32,
                "x": 32,
                "y": 59
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ed45ec6f-2624-4b64-9d9c-561e5282056c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 55,
                "offset": 2,
                "shift": 32,
                "w": 28,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e0813bc2-eb27-4f38-ab25-b0b0109376cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 55,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 464,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "54bf224c-5009-46a2-ba79-d88b873a3241",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 55,
                "offset": 3,
                "shift": 35,
                "w": 28,
                "x": 434,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "62f6a869-1319-4ed3-a28d-c5984efe487b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 55,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 400,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "6753afb7-7a0d-4daf-8abc-ce45ffe232b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 55,
                "offset": 0,
                "shift": 45,
                "w": 45,
                "x": 353,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "86812e90-bdbb-48c2-826f-52e0816936cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 55,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 319,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "555e8ac7-ddb1-423d-bb7e-1b36a30508fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 55,
                "offset": 0,
                "shift": 32,
                "w": 32,
                "x": 285,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "2445650c-b3bc-4631-9a5b-763d0df80c15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 55,
                "offset": 0,
                "shift": 29,
                "w": 29,
                "x": 254,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "4697d018-0a01-4a29-98ce-cffcd6d3565d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 55,
                "offset": 3,
                "shift": 13,
                "w": 10,
                "x": 66,
                "y": 59
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "3d0aea86-2a9e-4b26-b9db-56e5c182a802",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 55,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "99590f6d-c410-4b8c-be56-08f0a0b5719c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 55,
                "offset": 0,
                "shift": 13,
                "w": 11,
                "x": 208,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0ba50e4f-c4f8-4375-b9d2-a31b86241325",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 55,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "cf9fc785-d234-4e38-8eb3-e78b0aa91817",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 55,
                "offset": -1,
                "shift": 27,
                "w": 29,
                "x": 154,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "7a036164-649d-44bf-9ec6-6a3019cfac15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 55,
                "offset": 2,
                "shift": 16,
                "w": 9,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "8c1ad9f7-adfb-462d-b3cd-0390be414312",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 55,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "134573fa-ec3b-4f77-a469-c6d31c7d1a05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 55,
                "offset": 3,
                "shift": 27,
                "w": 22,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "88f82219-0f3c-4223-a421-de9dc1598ff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 55,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "7a6c357a-c2e9-482e-938d-9c28a3e8ffff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 55,
                "offset": 1,
                "shift": 27,
                "w": 23,
                "x": 43,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f17bc571-1593-43ca-8ba1-b90561d24183",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 55,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "68b1f75b-e046-4428-8055-10d2b1e7fcca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 55,
                "offset": 0,
                "shift": 13,
                "w": 15,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "54875fd7-814c-4db5-8b5c-22864877695c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 55,
                "offset": 1,
                "shift": 27,
                "w": 23,
                "x": 114,
                "y": 59
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "f3b924fa-d46b-4f1b-94ff-284f47b13713",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 55,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 348,
                "y": 59
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "ff10da64-4025-4b5b-96bb-06f49749bca7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 55,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 139,
                "y": 59
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e4240e88-88ed-4b80-aef9-1993194cf6da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 55,
                "offset": -3,
                "shift": 11,
                "w": 11,
                "x": 68,
                "y": 116
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "b27d8bc1-b5a5-4922-9c75-17d4f67a72ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 55,
                "offset": 3,
                "shift": 24,
                "w": 21,
                "x": 45,
                "y": 116
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "10726d73-1f26-4082-8880-b6c56b720eeb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 55,
                "offset": 3,
                "shift": 11,
                "w": 5,
                "x": 38,
                "y": 116
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "b379f83e-697e-4dce-8bbb-807e303b2988",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 55,
                "offset": 3,
                "shift": 40,
                "w": 34,
                "x": 2,
                "y": 116
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "62c8d258-99f2-4f86-a833-e9f5b433c12f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 55,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 477,
                "y": 59
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "08ed49ad-4003-4c4a-9ff2-f454218c07f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 55,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 451,
                "y": 59
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "02cddb6d-040e-4f53-bbea-ef5b328a1d54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 55,
                "offset": 3,
                "shift": 27,
                "w": 22,
                "x": 427,
                "y": 59
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ade61e45-8fce-4846-9356-ab6bf2102a1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 55,
                "offset": 1,
                "shift": 27,
                "w": 23,
                "x": 402,
                "y": 59
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "ec0c5b4b-aa3e-481e-8cc9-484306bd0df9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 55,
                "offset": 3,
                "shift": 16,
                "w": 14,
                "x": 386,
                "y": 59
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "2f3cc41b-d106-4585-a18a-fd98c3280749",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 55,
                "offset": 1,
                "shift": 24,
                "w": 22,
                "x": 81,
                "y": 116
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "8930755f-a3ac-4173-90bf-88ea0adb1555",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 55,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 371,
                "y": 59
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b385fcb9-c1fa-44a1-8a87-f3d6f3215af8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 55,
                "offset": 3,
                "shift": 27,
                "w": 21,
                "x": 325,
                "y": 59
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "ba8c09d4-ada8-4528-90ec-a8d8ca5618f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 55,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 299,
                "y": 59
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "850f5a83-4a86-4b71-9f49-f21be195a37b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 55,
                "offset": 0,
                "shift": 35,
                "w": 35,
                "x": 262,
                "y": 59
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "b6183010-bb8a-484d-a194-9041978d1aa1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 55,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 236,
                "y": 59
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "084adc66-afe3-4cee-8eed-6d338b22d83a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 55,
                "offset": 0,
                "shift": 24,
                "w": 24,
                "x": 210,
                "y": 59
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "f660531e-a9ad-42ea-b06c-504d6db6be3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 55,
                "offset": 0,
                "shift": 24,
                "w": 23,
                "x": 185,
                "y": 59
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "0cc66d0c-7075-4cd9-a2b1-b555dd8c8f5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 55,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 169,
                "y": 59
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "74d9bca2-a3b0-4b26-87e3-00e8de0d7030",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 55,
                "offset": 4,
                "shift": 12,
                "w": 5,
                "x": 162,
                "y": 59
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "063498f9-91c1-4e38-84b8-2149a119b0c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 55,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 146,
                "y": 59
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "ffcb0f07-1edb-4a5b-af1c-959932a843a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 55,
                "offset": 2,
                "shift": 28,
                "w": 25,
                "x": 292,
                "y": 230
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "1444573c-43f5-40bc-b091-cb1b0d2b0717",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 55,
                "offset": 9,
                "shift": 29,
                "w": 11,
                "x": 319,
                "y": 230
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "68a51f85-f109-4c03-b28b-9a2e909cecb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 65
        },
        {
            "id": "342d1ba6-7829-41cc-869b-f811bee7a810",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 84
        },
        {
            "id": "6b0fc190-5017-4702-b538-dfc8d3682fd7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 89
        },
        {
            "id": "536a04da-bd16-4344-93bc-6f5383da46c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 902
        },
        {
            "id": "7c9fa273-2029-4c6f-9798-8053c1f288ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 913
        },
        {
            "id": "dc448823-8c65-48a3-9e02-29f38607ae60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 916
        },
        {
            "id": "d2e27b19-8b4f-4f3a-872c-37f9e3f1362f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 32,
            "second": 923
        },
        {
            "id": "969af9a0-24fb-41fc-9327-6f658d3933a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 932
        },
        {
            "id": "79d1161c-a5cc-4ea4-bdcd-f87f440c1f0d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 933
        },
        {
            "id": "8a29b2f2-e699-45e4-aca2-b487acec144a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 939
        },
        {
            "id": "24efa6d6-2deb-4a08-b95b-558ca4a875c5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 49,
            "second": 49
        },
        {
            "id": "4632c9fe-7ee4-4a66-9ae9-08375606e8dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 32
        },
        {
            "id": "9e792d51-d8fc-4208-96eb-6cebd0638096",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 84
        },
        {
            "id": "58107cf8-b9d8-4ac4-914e-6a597294d397",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 86
        },
        {
            "id": "f88780c2-52f5-460f-81fe-2a1743e27da1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 87
        },
        {
            "id": "654bb3da-e38e-442f-91e8-b0ed345d46ac",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 89
        },
        {
            "id": "4cbf7fc9-1236-4ef0-9c7f-0c216750753b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "0a6f28c5-20c7-4a10-b2ec-03fcb336ef44",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "72061b94-b3f4-4355-b348-d5c95cc3fcb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "1c4c8b67-a35f-4e22-89f2-2455dfc1cb07",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 160
        },
        {
            "id": "83e86bc0-6804-473f-ad08-f61bd781b1c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 65,
            "second": 8217
        },
        {
            "id": "26b39bba-6b02-40a8-baf5-3e6d374416fc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 44
        },
        {
            "id": "67ea5b24-8b3f-4037-8b90-cbc7121c3791",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 46
        },
        {
            "id": "3256374c-0c27-4083-9761-bd853e03e303",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 65
        },
        {
            "id": "e661da86-e140-4ba1-a51e-83901e282f8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 32
        },
        {
            "id": "2f055992-4708-425e-b647-4e69273d0fc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 84
        },
        {
            "id": "000d2865-3cfc-4518-acf2-5cc3998088cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 86
        },
        {
            "id": "654fd48a-9bb7-472a-aeb5-9ea2ac936294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 87
        },
        {
            "id": "aaa5ec7d-70bd-4105-b7c8-7666aa824ced",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 76,
            "second": 89
        },
        {
            "id": "79a53fad-f6af-4b37-9a60-3995f530b0f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 121
        },
        {
            "id": "8fb5d8ca-6a64-4c63-accf-38369476298f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 160
        },
        {
            "id": "1b8746b9-02b2-4a71-9486-b05cef155052",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8217
        },
        {
            "id": "ce32816f-a259-4703-8075-c89b648cd3ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 32
        },
        {
            "id": "52d49a95-6789-4a4e-bcad-b5f7f20672fd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 44
        },
        {
            "id": "a817d959-5add-4816-b4cd-16d3fff77411",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 80,
            "second": 46
        },
        {
            "id": "7bda6ff7-3af3-435c-838e-ce9d1e07f66a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 65
        },
        {
            "id": "0e4195ee-cb64-46f9-a0d5-7dc88c97387c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 160
        },
        {
            "id": "2cc8aff9-0326-4b80-8b2d-b16fae3a8222",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 84
        },
        {
            "id": "eef0d309-8262-4578-960e-38d8a406a838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 86
        },
        {
            "id": "d5a89baa-2ec1-47dd-84de-eb32934233bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 87
        },
        {
            "id": "3ada3cb4-73cd-46e5-b98b-ba9ae10ca429",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 89
        },
        {
            "id": "b8329dc5-5411-47c8-ba5c-d4a62cc4f37a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 32
        },
        {
            "id": "5ee54061-0f62-43ba-bbf6-33869f0b0107",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 44
        },
        {
            "id": "e701a295-88b7-4b2c-990d-8f564eb52916",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 45
        },
        {
            "id": "b2e74323-6480-4933-a5fb-2dbcaf50d1fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 46
        },
        {
            "id": "ad0cfee7-e6e0-4055-ba3c-bb33469da2c0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 58
        },
        {
            "id": "2fd7a6c5-8b7e-4fce-b599-1922a5c5953f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 59
        },
        {
            "id": "bac2a3b3-cfb6-4ee8-b9f4-cf51a2aa9fd1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 84,
            "second": 65
        },
        {
            "id": "ac519ae0-b0e9-4e06-9783-b1ef69fa0fd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 79
        },
        {
            "id": "f769625a-7a92-40e9-8f43-3a31436033e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 97
        },
        {
            "id": "b79ef5ab-5d01-42df-ab07-e3973e0cb361",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 99
        },
        {
            "id": "8e174c0c-d3f0-4400-8f61-9185b2036fe0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 101
        },
        {
            "id": "8f73f5c4-dff5-4e63-9cfb-ca912f9c86f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 105
        },
        {
            "id": "ab5108c8-88aa-4ca4-8a07-99af31090c01",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 111
        },
        {
            "id": "a7551997-9b7a-4ba4-8f23-f2c52c36f08d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 114
        },
        {
            "id": "88394bc8-66b4-4efb-ab7f-82c192e00c18",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 115
        },
        {
            "id": "835c12e5-1a21-4785-95af-aa7389269e16",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 117
        },
        {
            "id": "31d71a44-a6c7-4ce8-968a-454727937564",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 119
        },
        {
            "id": "0d099f54-e613-4028-a2b9-f2efbcce8686",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 121
        },
        {
            "id": "6c5f4ff9-c89b-45a2-946e-7322db08c85c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 160
        },
        {
            "id": "cc5e055c-c429-45f6-95fe-b841dae8f62a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 173
        },
        {
            "id": "2958b14d-56e7-4716-86c0-47a07c509ef6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 894
        },
        {
            "id": "8d017842-918e-4149-a0e7-9fb9d6054a42",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 44
        },
        {
            "id": "39a0d79a-3a4b-4f07-9e24-ac38786decd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 45
        },
        {
            "id": "185c1d4a-5edc-4bfa-adaf-adea904a70f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 46
        },
        {
            "id": "af1f9c73-c476-4795-97bd-4d3b5e2ac647",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 58
        },
        {
            "id": "546e9269-7bfe-4d31-9b43-6d43c61c4bdb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 59
        },
        {
            "id": "090e77c4-6d36-43f1-a213-c11beb72c8b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 65
        },
        {
            "id": "7bccc9ef-8bf6-47a3-a78e-2bb01ae031ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 86,
            "second": 97
        },
        {
            "id": "961e99af-f9b4-49ba-a1b4-7abcd3dbd498",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 101
        },
        {
            "id": "a18c275f-8ab9-47a8-8075-5323286e4e48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "67b742ca-2d33-4345-92ba-f530bb53966a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 111
        },
        {
            "id": "53e2b33f-37e2-4d9a-aa8a-2b686642d8f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 114
        },
        {
            "id": "51523f7f-09f6-4d09-972a-c3e104f05d20",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 117
        },
        {
            "id": "21694d18-bd47-4ec9-8cc4-98fd7f2d06ff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 121
        },
        {
            "id": "ac27fa8c-fc27-4a06-b57d-c1cb57eb4c46",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 173
        },
        {
            "id": "6c684341-14a8-4b9e-a6dd-68e1729b87a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 894
        },
        {
            "id": "4deec43d-5845-4288-8e38-6d6b252f35b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 44
        },
        {
            "id": "5b0d7fc7-6aa0-405b-a26e-97b9f27fd0a4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "75737710-e686-4e0e-bcc4-d8d900a637e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 46
        },
        {
            "id": "2f4fa32c-5767-4ffa-a1f7-faa9fc11c3b3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "cd1bd3c1-463f-475f-87c8-991c8e3e6991",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "277c4206-ba86-4115-956e-d25d94b89286",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 65
        },
        {
            "id": "b03c3323-5d91-425d-80e3-8891109cbff5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 97
        },
        {
            "id": "d6792e6c-8ea9-431c-ba9d-de17b8c8a1e5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 101
        },
        {
            "id": "b7d7d059-a169-4538-a7ec-efabe7e68fa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 111
        },
        {
            "id": "d67b96b0-c5a1-4ad1-919b-b7ca8a63c9d6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "8efee99a-983e-43ff-b624-39a91353557a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "8935423b-d0af-4f22-adff-2038743c42da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "ee49d67d-d3ee-4813-950c-72787f582fea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 894
        },
        {
            "id": "dac3dd18-ead1-4c7b-80c0-6df07d695af9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 32
        },
        {
            "id": "e755db0d-af1b-4ed4-85cf-451ee5973b34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 44
        },
        {
            "id": "04604226-ff6f-429a-8b4d-322878b5001d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 45
        },
        {
            "id": "0392b144-8adf-4b4c-b765-310cd6b6fe53",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -6,
            "first": 89,
            "second": 46
        },
        {
            "id": "a7a26ead-94bb-4c2c-ac92-b1ea7fbbbfd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 58
        },
        {
            "id": "543b8c90-fa06-4576-be4e-456f36602778",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 59
        },
        {
            "id": "d1e74a53-c7c1-4da2-8f7b-bb3679689279",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 65
        },
        {
            "id": "ecfb2e24-b774-4691-a9ef-1d06081ac547",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 97
        },
        {
            "id": "c63aee22-a1df-4304-b5d2-cdf5239051bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 101
        },
        {
            "id": "5d626225-c2a4-4ce5-b641-bc897a7a9d45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 105
        },
        {
            "id": "8d58c8c0-86f4-492d-afdb-f2152dced9b7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 111
        },
        {
            "id": "77bf03d8-b3b4-439e-ba7a-e0502e6b5e67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 112
        },
        {
            "id": "6a9f8477-d382-4737-b939-83044920a723",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 113
        },
        {
            "id": "89af0c13-3ede-4c81-9e0c-1cf4c63f8825",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 117
        },
        {
            "id": "2d35938d-a320-444a-9f7c-ea8539be40d0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 118
        },
        {
            "id": "2faa1d02-2ebc-4f82-923f-be6571bd29ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 160
        },
        {
            "id": "05bddcfb-d191-4be0-b29f-a608a1400ced",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 89,
            "second": 173
        },
        {
            "id": "c887498f-ce5d-4601-b527-7a611fcfe03b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 89,
            "second": 894
        },
        {
            "id": "53bcb000-2011-4e28-b01d-d8440b87b9f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "b0793273-acfd-417e-ba7a-f2a76ae727f0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 102,
            "second": 8217
        },
        {
            "id": "8ab22416-8238-400a-907d-f9c61d623f54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 44
        },
        {
            "id": "a2f71d8e-a5bc-46ef-9708-e887cb790cfd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 114,
            "second": 46
        },
        {
            "id": "f17c9619-d0e6-47e3-a82a-8100b304e9a6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 114,
            "second": 8217
        },
        {
            "id": "f8b30a68-087a-4bd7-abff-81f1f787073c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 44
        },
        {
            "id": "ebf2371c-04ee-4661-8266-b5f6fa4cfa36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 118,
            "second": 46
        },
        {
            "id": "03debe92-bd80-4909-8670-91e9989cbcc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 44
        },
        {
            "id": "b86130b6-38f9-45bf-8243-6ecfcd26d661",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 119,
            "second": 46
        },
        {
            "id": "e5a1b305-ae53-42da-b4a2-a9d9bb7660c7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 44
        },
        {
            "id": "c44f7721-0ccd-48f3-8f69-331a05c9ca52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 121,
            "second": 46
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 36,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}